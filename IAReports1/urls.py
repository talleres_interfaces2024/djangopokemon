from django.contrib import admin
from django.urls import path
#from report import views

# urlpatterns = [
#     path('admin/', admin.site.urls),
#     path('dash/', views.index, name="dash"),
# ]

#from . import views

# Importa la vista cargar_xlsx desde views.py
from report.views import cargar_xlsx

urlpatterns = [
    # Asegúrate de usar la vista cargar_xlsx para la URL 'cargar-xlsx/'
    path('cargar-xlsx/', cargar_xlsx, name='cargar_xlsx'),
]

