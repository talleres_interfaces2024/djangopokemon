from django.db import models

class ConexionDB(models.Model):
    nombre = models.CharField(max_length=120)
    #edad = models.IntegerField()
    aciertos = models.IntegerField()
    desaciertos = models.IntegerField()
    puntaje = models.IntegerField()

    def __str__(self):
        return self.nombre
