# En views.py dentro de la aplicación report (o la aplicación donde quieras incluir esta funcionalidad)
from django.shortcuts import render
import pandas as pd
import plotly.express as px
from .models import ConexionDB

def cargar_xlsx(request):
    if request.method == 'POST' and request.FILES['xlsx_file']:
        # Cargar y procesar el archivo XLSX con openpyxl como motor
        xlsx_file = request.FILES['xlsx_file']
        df = pd.read_excel(xlsx_file, engine='openpyxl')

        # Guardar los datos del XLSX en la base de datos (si es necesario)
        # Comenta esta parte si no deseas guardar los datos en la base de datos
        for index, row in df.iterrows():
            ConexionDB.objects.create(
                nombre=row['nombre'],
                aciertos=row['aciertos'],
                desaciertos=row['desaciertos'],
                puntaje=row['puntaje']
            )

        # Generar el gráfico con Plotly
        fig = px.bar(df, x="nombre", y="puntaje", color="desaciertos", barmode="group")
        plot_div = fig.to_html(full_html=False)

        # Renderizar la plantilla y pasar el gráfico como contexto
        return render(request, 'report/carga_exitosa.html', context={'plot_div': plot_div})
    else:
        return render(request, 'report/cargar_xlsx.html')



# from django.shortcuts import render
# import pandas as pd
# import plotly.express as px

# def index(request):
#     df = pd.DataFrame({
#         "nombre": ["Daniel", "Esteban", "Viviana", "Marcela"],
#         "puntaje": [4,2,4,5],
#         "desaciertos": ["14", "15", "8", "6"]
#     })


#     fig =  px.bar(df, x="nombre", y="puntaje", color="desaciertos", barmode="group")

#     plot_div = fig.to_html(full_html=False)

#     return render(request, "report/index.html", context={'plot_div': plot_div, 'table': df.to_html()})

# En views.py dentro de la aplicación report (o la aplicación donde quieras incluir esta funcionalidad)


# En views.py dentro de la aplicación report (o la aplicación donde quieras incluir esta funcionalidad)


# En views.py dentro de la aplicación report (o la aplicación donde quieras incluir esta funcionalidad)
